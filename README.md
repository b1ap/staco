[![pipeline report](https://gitlab.com/b1ap/staco/badges/master/pipeline.svg)](https://gitlab.com/b1ap/staco/-/commits/master)
[![coverage report](https://gitlab.com/b1ap/staco/badges/master/coverage.svg)](https://gitlab.com/b1ap/staco/-/commits/master)


# STACO
#### Student and Teaching Assistant Connector

STACO adalah aplikasi web yang berfungsi untuk menghubungkan antara asdos dan mahasiswa. Aplikasi ini memiliki fitur grouping yang didalamnya dapat digunakan untuk melakukan penjadwalan demo, penjadwalan asistensi, dan memberikan broadcast kepada mahasiswa.



#### Our Developers !
- Athiya Fatihah Akbar (1806186824)
- Julian Fernando (1906285522)
- Danan Maulidan Akbar (1906293000)
- Muhammad Ikhsan Asa Pambayun (1906350830)
- Muhammad Fayaad (1906398433)

Link Web : https://staco-b1.herokuapp.com/
