$(document).ready(function () {
    getmatkul();

});

function getmatkul() {
    var nama = ""
    $.ajax({
		url : "/matkul/dev/api",
		success : function(data){
			var matkullist = data
			if (matkullist.length === 0 ){
                $("#matkullist").append("<h4 class=\"text-center\">TIDAK ADA MATA KULIAH TERDAFTAR</h4>");
			}
			else{
			    var result = ""
			    result += "<ul class=\"list-group\">"
                for (i = 0; i < matkullist.length; i++){
                    result += "<li class=\"list-group-item\">"
                    var nama = matkullist[i].nama_matkul
                    var kode = matkullist[i].kode_matkul
                    var id = matkullist[i].id
                    var url = "/matkul/dev/info/" + id
                    var teks = " [" + kode + "] " + nama
                    result = result + "<a href=\"" + url
                    result = result + "\"><button type=\"button\" class=\"btn btn-primary\">EDIT</button></a>"
                    result += teks;
                    result += "</li>";
                }
                result += "</ul>";
                $("#matkullist").append(result);
			}
		}
	})
}