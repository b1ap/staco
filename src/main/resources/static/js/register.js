var listUsers;
var dbError = false;

function getUsers() {
    $.ajax({
        url: '/auth/get-users',
        success: function(result) {
            console.log(result);
            listUsers = result;
        },
        error: function() {
            dbError = true;
        }
    })
}

document.getElementById("usernameField").oninput = usernameChecker;

function usernameChecker() {
    getUsers();

    var newUsername = document.getElementById("usernameField").value;

    if (dbError) {
        return false;
    } else {
        for (i = 0; i < listUsers.length; i++) {
            var temp = listUsers[i].username;

            if (temp == newUsername) {
                document.getElementById("usernameError").innerHTML = "Username sudah terdaftar";
                return false;
            }
        }
    }

    document.getElementById("usernameError").innerHTML = "";
    return true;
}

document.getElementById("NPMField").oninput = npmChecker;

function npmChecker() {
    getUsers();

    var newNPM = document.getElementById("NPMField").value;

    if (dbError) {
        return false;
    } else {
        for (i = 0; i < listUsers.length; i++) {
            var temp = listUsers[i].npm;

            if (temp == newNPM) {
                document.getElementById("npmError").innerHTML = "NPM sudah terdaftar";
                return false;
            }
        }
    }

    document.getElementById("npmError").innerHTML = "";
    return true;
}

document.getElementById("emailField").oninput = emailChecker;

function emailChecker() {
    getUsers();

    var newEmail = document.getElementById("emailField").value;

    if (dbError) {
        return false;
    } else {
        for (i = 0; i < listUsers.length; i++) {
            var temp = listUsers[i].email;

            if (temp == newEmail) {
                document.getElementById("emailError").innerHTML = "Email sudah terdaftar";
                return false;
            }
        }
    }

    document.getElementById("emailError").innerHTML = "";
    return true;
}

document.getElementById("re-passwordField").oninput = passChecker;

function passChecker() {
    var pass1 = document.getElementById("passwordField").value;
    var pass2 = document.getElementById("re-passwordField").value;

    console.log(pass1);
    console.log(pass2);

    if (pass1 != pass2) {
        document.getElementById("passwordError").innerHTML = "Password tidak sama";
        return false;
    } else {
        document.getElementById("passwordError").innerHTML = "";
        return true;
    }
}

const form = document.getElementById('registerForm');

form.addEventListener('submit', (e) => {
    if (dbError) {
        alert("Maaf database sedang down");
        e.preventDefault();
    }

    if (!usernameChecker()) {
        alert("Tolong masukkan username yang valid");
        e.preventDefault();
    }

    if (!npmChecker()) {
        alert("Tolong masukkan NPM yang valid");
        e.preventDefault();
    }

    if (!emailChecker()) {
        alert("Tolong masukkan email yang valid");
        e.preventDefault();
    }

    if (!passChecker()) {
        alert("Tolong masukkan password yang valid");
        e.preventDefault();
    }
})