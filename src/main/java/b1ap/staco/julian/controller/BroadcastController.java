package b1ap.staco.julian.controller;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import b1ap.staco.julian.service.BroadcastServiceImpl;
import b1ap.staco.julian.core.Broadcast;
import b1ap.staco.fayaad.model.Matkul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.ArrayList;

@Controller
@RequestMapping("/broadcast")
public class BroadcastController {
    @Autowired
    private BroadcastServiceImpl broadcastService;

    @Autowired
    private UserRepository userRepository;

    private String username;
    private Matkul mataKuliah;
    private List<Broadcast> broadcasts = new ArrayList<>();
    private String idStr;
    private int id;
    private User userSession;

    @GetMapping("/")
    public String getListMataKuliah(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        username = auth.getName();
        userSession = userRepository.findByUsername(auth.getName());
        model.addAttribute("allMatkul", broadcastService.getAllMatkulByUsername(username));
        model.addAttribute("allBroadcast", broadcasts);
        model.addAttribute("isAsdos", broadcastService.checkAsdos(username, mataKuliah));
        model.addAttribute("user", userSession);
        return "julian/chooseMatkul";
    }

    @PostMapping("/get-broadcast")
    public String getAllBroadcast(HttpServletRequest request) {
        idStr = request.getParameter("idString");
        id = Integer.parseInt(idStr);
        mataKuliah = broadcastService.getMatkulByID(id);
        broadcasts = broadcastService.getAllBroadcastByMataKuliah(mataKuliah);
        return "redirect:/broadcast/";
    }

    @PostMapping("/create-broadcast")
    public String createBroadcast(Model model) {
        model.addAttribute("broadcast", new Broadcast());
        model.addAttribute("namaMataKuliah", mataKuliah.getNama_matkul());
        model.addAttribute("userInfo", broadcastService.getUserInfoByUsername(username));
        model.addAttribute("user", userSession);
        return "julian/broadcastForm";
    }

    @PostMapping("/add-broadcast")
    public String addBroadcast(@ModelAttribute("broadcast") Broadcast broadcast) {
        broadcast.setMataKuliah(mataKuliah);
        broadcast.setUser(broadcastService.getUserByUsername(username));
        broadcastService.createBroadcast(broadcast);
        broadcasts = broadcastService.getAllBroadcastByMataKuliah(mataKuliah);
        return "redirect:/broadcast/";
    }
}