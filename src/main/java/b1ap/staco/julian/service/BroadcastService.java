package b1ap.staco.julian.service;

import b1ap.staco.julian.core.Broadcast;
import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.ikhsan.model.User;

import java.util.List;

public interface BroadcastService {
    Broadcast createBroadcast(Broadcast broadcast);
    List<Matkul> getAllMatkulByUsername(String username);
    List<Broadcast> getAllBroadcastByMataKuliah(Matkul mataKuliah);
    Matkul getMatkulByID(int id);
    boolean checkAsdos(String username, Matkul mataKuliah);
    String getUserInfoByUsername(String username);
    User getUserByUsername(String username);
}