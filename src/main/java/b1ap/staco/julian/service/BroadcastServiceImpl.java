package b1ap.staco.julian.service;

import b1ap.staco.julian.core.Broadcast;
import b1ap.staco.julian.repository.BroadcastRepository;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

@Service
public class BroadcastServiceImpl implements BroadcastService {
    @Autowired
    private BroadcastRepository broadcastRepository;
    @Autowired
    private MendaftarRepository mendaftarRepository;
    @Autowired
    private MatkulRepository matkulRepository;
    @Autowired
    private UserRepository userRepository;

    public Broadcast createBroadcast(Broadcast broadcast) {
        broadcastRepository.save(broadcast);
        return broadcast;
    }

    public List<Matkul> getAllMatkulByUsername(String username) {
        User user = userRepository.findByUsername(username);
        List<Mendaftar> allMendaftar = mendaftarRepository.findAllByUser(user);
        List<Matkul> result = new ArrayList<>();
        for (Mendaftar mendaftar : allMendaftar) {
            Matkul mataKuliah = mendaftar.getMatkul();
            result.add(mataKuliah);
        }
        return result;
    }

    public List<Broadcast> getAllBroadcastByMataKuliah(Matkul mataKuliah) {
        return broadcastRepository.findAllByMataKuliah(mataKuliah);
    }

    public Matkul getMatkulByID(int id) {
        return matkulRepository.findByID(id);
    }

    public boolean checkAsdos(String username, Matkul mataKuliah) {
        if ((username == null) || (mataKuliah == null))
            return false;
        User user = userRepository.findByUsername(username);
        List<Mendaftar> allMendaftar = mendaftarRepository.findAllByUser(user);
        for (Mendaftar mendaftar : allMendaftar) {
            Matkul matkulTerdaftar = mendaftar.getMatkul();
            if (mataKuliah.getID() == matkulTerdaftar.getID()) {
                return mendaftar.getIsAsdos();
            }
        }
        return false;
    }

    public String getUserInfoByUsername(String username) {
        User user = userRepository.findByUsername(username);
        return (user.getfName() + " " + user.getlName() + " - " + user.getNPM());
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}