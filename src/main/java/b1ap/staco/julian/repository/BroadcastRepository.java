package b1ap.staco.julian.repository;

import b1ap.staco.julian.core.Broadcast;
import b1ap.staco.fayaad.model.Matkul;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BroadcastRepository extends JpaRepository<Broadcast, Matkul> {
    List<Broadcast> findAllByMataKuliah(Matkul mataKuliah);
}