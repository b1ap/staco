package b1ap.staco.julian.core;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.ikhsan.model.User;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "broadcast")
@NoArgsConstructor
public class Broadcast {
    @Id
    @Column(name = "broadcastId")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int broadcastId;

    @Column(name = "message", nullable = false)
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "username", nullable = false)
    private User user;

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID", nullable = false)
    private Matkul mataKuliah;

    public int getBroadcastId() {
        return broadcastId;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public Matkul getMataKuliah() {
        return mataKuliah;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMataKuliah(Matkul mataKuliah) {
        this.mataKuliah = mataKuliah;
    }
}