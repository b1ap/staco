package b1ap.staco.controller;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ui.Model;

@Controller
public class MainController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String home(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());
        model.addAttribute("user", userSession);
        return "home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/jadwalasis")
    private String jadwalAsis(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());
        model.addAttribute("user", userSession);
        return "JadwalAsis";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/jadwaldemo")
    private String jadwalDemo(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());
        model.addAttribute("user", userSession);
        return "JadwalDemo";
    }
}
