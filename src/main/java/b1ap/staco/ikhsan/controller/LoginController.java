package b1ap.staco.ikhsan.controller;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/auth")
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String showLoginPage() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());

        if (userSession != null) {
            return "redirect:/";
        } else {
            return "ikhsan/LoginPage";
        }
    }

    @GetMapping("/logout")
    public String logoutHandle() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());

        if (userSession != null) {
            return "redirect:/logout";
        } else {
            return "redirect:/";
        }
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userService.getUserByUsername(auth.getName());

        if (userSession != null) {
            return "redirect:/";
        } else {
            model.addAttribute("user", new User());

            return "ikhsan/RegisterForm";
        }
    }

    @PostMapping("/register")
    public String submitRegister(User user) {
        userService.addNewUser(user);

        return "redirect:/auth/login";
    }

    @GetMapping(path = "/get-users", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<User>> getListMahasiswa() {
        return ResponseEntity.ok(userService.getListUsers());
    }
}
