package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Iterable<User> getListUsers() {
        return userRepository.findAll();
    }

    @Override
    public User addNewUser(User user) {
        BCryptPasswordEncoder passEncoder = new BCryptPasswordEncoder();
        String encodedPass = passEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);

        user.setIsAdmin(false);

        if (user.getUsername().equals("admin")){
            user.setIsAdmin(true);
        }

        userRepository.save(user);

        return user;
    }
}
