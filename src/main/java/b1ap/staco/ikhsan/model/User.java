package b1ap.staco.ikhsan.model;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="users")
@NoArgsConstructor
public class User {

    @Id
    @Column(name = "username" ,nullable = false, updatable = false)
    private String username;

    @Column(name = "password" ,nullable = false)
    private String password;

    @Column(name = "fName" ,nullable = false)
    private String fName;

    @Column(name = "lName" ,nullable = false)
    private String lName;

    @Column(name = "email" ,nullable = false)
    private String email;

    @Column(name = "isAdmin" ,nullable = false)
    private Boolean isAdmin;

    @Column(name = "NPM", unique = true)
    private String NPM;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "user")
    private Set<Mendaftar> matkulDidaftar = new HashSet<>();

    public User(String username, String email, String password, String fName, String lName, boolean isAdmin, String NPM) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.fName = fName;
        this.lName = lName;
        this.isAdmin = isAdmin;
        this.NPM = NPM;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getEmail() { return email; }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public String getNPM() { return NPM; }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) { this.password = password; }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public void setEmail(String email) { this.email = email; }

    public void setIsAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public void setNPM(String NPM) {
        this.NPM = NPM;
    }

    public Set<Mendaftar> getMatkulDidaftar() {
        return matkulDidaftar;
    }

}
