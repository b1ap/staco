package b1ap.staco.fayaad.controller;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.service.MatkulService;
import b1ap.staco.fayaad.service.MendaftarService;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping(path = "/matkul")
public class MatkulController {
    @Autowired
    private MatkulService matkulService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MendaftarService mendaftarService;

    @GetMapping(path = "")
    public String getListMatkul(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            model.addAttribute("user", userSession);
            model.addAttribute("MatkulList", matkulService.getMatkulByUsername(userSession.getUsername()));
            return "fayaad/matkul";
        }
        return "redirect:/login";
    }

    @GetMapping(path = "/join")
    public String joinMatkulGet(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            model.addAttribute("MatkulList", matkulService.getListMatkul());
            model.addAttribute("user", userSession);
            return "fayaad/matkul_join";
        }
        return "redirect:/login";
    }

    @PostMapping(path = "/join")
    public String joinMatkulPost(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            String id = request.getParameter("id");
            Matkul matkul = matkulService.getMatkul(id);
            mendaftarService.createPendaftaran(new Mendaftar(), matkul, userSession);
            return "redirect:/matkul/";
        }
        return "redirect:/login";
    }

    @GetMapping(path = "/info/{id}")
    public String getMatkul(@PathVariable(value = "id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            Matkul matkul = matkulService.getMatkul(id);
            Mendaftar mendaftar = mendaftarService.getMendaftarByUserMatkul(userSession, matkul);
            model.addAttribute("pendaftar", mendaftar);
            model.addAttribute("matkul", matkul);
            model.addAttribute("mendaftarList", mendaftarService.getListMendaftar(matkul));
            model.addAttribute("user", userSession);
            return "fayaad/matkul_info";
        }
        return "redirect:/login";
    }

    @GetMapping(path = "/info/{id}/add/{idmahasiswa}/{idasdos}")
    public String addGroup(@PathVariable(value = "id") String id,
                           @PathVariable(value = "idmahasiswa") String idmahasiswa,
                           @PathVariable(value = "idasdos") String idasdos) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            Matkul matkul = matkulService.getMatkul(id);
            Mendaftar mendaftar = mendaftarService.getMendaftarByUserMatkul(userSession, matkul);
            if (String.valueOf(mendaftar.getIdmendaftar()).equals(idasdos)) {
                mendaftarService.addGroup(idmahasiswa, idasdos);
            }
            return "redirect:/matkul/info/" + id;
        }
        return "redirect:/login";

    }

    @GetMapping(path = "/info/{id}/drop/{idmahasiswa}/{idasdos}")
    public String dropGroup(@PathVariable(value = "id") String id,
                           @PathVariable(value = "idmahasiswa") String idmahasiswa,
                           @PathVariable(value = "idasdos") String idasdos) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            Matkul matkul = matkulService.getMatkul(id);
            Mendaftar mendaftar = mendaftarService.getMendaftarByUserMatkul(userSession, matkul);
            if(String.valueOf(mendaftar.getIdmendaftar()).equals(idasdos)){
                mendaftarService.dropGroup(idmahasiswa);
            }
            return "redirect:/matkul/info/" + id;
        }
        return "redirect:/login";
    }

    @PostMapping(path = "/setkode_asdos")
    public String setKodeAsdos(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            String id = request.getParameter("id");
            String idasdos = request.getParameter("idasdos");
            String kode_asdos = request.getParameter("kode_asdos");
            Matkul matkul = matkulService.getMatkul(id);
            Mendaftar mendaftar = mendaftarService.getMendaftarByUserMatkul(userSession, matkul);
            if(String.valueOf(mendaftar.getIdmendaftar()).equals(idasdos)) {
                mendaftarService.setKodeAsdos(idasdos, kode_asdos);
            }
            return "redirect:/matkul/info/" + id;
        }
        return "redirect:/login";
    }

    @GetMapping(path = "/info/{id}/{idmendaftar}/leave")
    public String leaveMatkul(@PathVariable(value = "id") String id, @PathVariable(value = "idmendaftar") String idmendaftar) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null) {
            mendaftarService.deleteMendaftar(idmendaftar);
            return "redirect:/matkul";
        }
        return "redirect:/login";
    }

}
