package b1ap.staco.fayaad.controller;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.service.MatkulService;
import b1ap.staco.fayaad.service.MendaftarService;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping(path = "/matkul/dev")
public class MatkulAdminController {
    @Autowired
    private MatkulService matkulService;
    @Autowired
    private MendaftarService mendaftarService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "")
    public String getListMatkulDev(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            model.addAttribute("user", userSession);
            return "fayaad/matkuldev";
        }
        return "redirect:/matkul";
    }

    @GetMapping(path = "/api", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getListMatkulAPI() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            return ResponseEntity.ok(matkulService.getListMatkul());
        }
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    @GetMapping(path = "/create")
    public String createMatkulDevGet(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            model.addAttribute("matkul", new Matkul());
            model.addAttribute("user", userSession);
            return "fayaad/matkuldev_create";
        }
        return "redirect:/matkul";
    }

    @PostMapping(path = "/create")
    public String createMatkulDevPost(@ModelAttribute Matkul matkul) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            matkulService.createMatkul(matkul);
            return "redirect:/matkul/dev";
        }
        return "redirect:/matkul";
    }

    @GetMapping(path = "/info/{id}")
    public String getMatkulDev(@PathVariable(value = "id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            Matkul matkul = matkulService.getMatkul(id);
            model.addAttribute("mendaftarList", mendaftarService.getListMendaftar(matkul));
            model.addAttribute("matkul", matkul);
            model.addAttribute("user", userSession);
            return "fayaad/matkuldev_info";
        }
        return "redirect:/matkul";
    }

    @GetMapping(path = "/set/{idmendaftar}")
    public String setAsdos(@PathVariable(value = "idmendaftar") String idmendaftar) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            Mendaftar mendaftar = mendaftarService.getMendaftar(idmendaftar);
            mendaftarService.setAsdos(mendaftar);
            int id = mendaftar.getMatkul().getID();
            return "redirect:/matkul/dev/info/" + id;
        }
        return "redirect:/matkul";
    }

    @GetMapping(path = "/drop/{idmendaftar}")
    public String dropAsdos(@PathVariable(value = "idmendaftar") String idmendaftar) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            Mendaftar mendaftar = mendaftarService.getMendaftar(idmendaftar);
            mendaftarService.dropAsdos(mendaftar);
            int id = mendaftar.getMatkul().getID();
            return "redirect:/matkul/dev/info/" + id;
        }
        return "redirect:/matkul";
    }

    @GetMapping(path = "/info/{id}/delete")
    public String deleteMatkulDev(@PathVariable(value = "id") String id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userSession = userRepository.findByUsername(auth.getName());
        if (userSession != null && userSession.getIsAdmin()) {
            matkulService.deleteMatkul(id);
            return "redirect:/matkul/dev/";
        }
        return "redirect:/matkul";
    }

}
