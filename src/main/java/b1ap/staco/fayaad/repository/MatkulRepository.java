package b1ap.staco.fayaad.repository;

import b1ap.staco.fayaad.model.Matkul;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatkulRepository extends JpaRepository<Matkul, String> {
    Matkul findByID(int id);

}