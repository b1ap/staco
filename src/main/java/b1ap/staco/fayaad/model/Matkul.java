package b1ap.staco.fayaad.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "matkuls")
@NoArgsConstructor
public class Matkul {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false)
    private int ID;

    @Column(name = "nama_matkul", nullable = false)
    private String nama_matkul;

    @Column(name = "kode_matkul", nullable = false)
    private String kode_matkul;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "matkul")
    private List<Mendaftar> matkulDidaftar = new ArrayList<>();

    public Matkul(String nama_matkul, String kode_matkul){
        this.nama_matkul = nama_matkul;
        this.kode_matkul = kode_matkul;
    }

    public String getNama_matkul() {
        return nama_matkul;
    }

    public String getKode_matkul() {
        return kode_matkul;
    }

    public int getID() {
        return ID;
    }

    public void setKode_matkul(String kode_matkul) {
        this.kode_matkul = kode_matkul;
    }

    public void setNama_matkul(String nama_matkul) {
        this.nama_matkul = nama_matkul;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public List<Mendaftar> getMatkulDidaftar() {
        return matkulDidaftar;
    }
}