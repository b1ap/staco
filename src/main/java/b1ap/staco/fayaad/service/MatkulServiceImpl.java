package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MatkulServiceImpl implements MatkulService {
    @Autowired
    private MatkulRepository matkulRepository;
    @Autowired
    private MendaftarRepository mendaftarRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Iterable<Matkul> getListMatkul(){
        return matkulRepository.findAll();
    }

    @Override
    public Matkul createMatkul(Matkul matkul){
        for (Matkul matkulloop:getListMatkul()){
            if (matkulloop.getKode_matkul().equals(matkul.getKode_matkul())){
                return null;
            }
        }
        matkulRepository.save(matkul);
        return matkul;

    }

    @Override
    public Matkul getMatkul(String id){
        return matkulRepository.findByID(Integer.parseInt(id));
    }

    @Override
    public Iterable<Matkul> getMatkulByUsername(String username){
        User user = userRepository.findByUsername(username);
        List<Matkul> matkuls = new ArrayList<>();
        for (Mendaftar mendaftar : user.getMatkulDidaftar()) {
            matkuls.add(mendaftar.getMatkul());
        }
        return matkuls;
    }

    @Override
    public void deleteMatkul(String id){
        Matkul matkul = matkulRepository.findByID(Integer.parseInt(id));
        for (Mendaftar mendaftar : matkul.getMatkulDidaftar()) {
            User user = mendaftar.getUser();
            user.getMatkulDidaftar().remove(mendaftar);
            mendaftarRepository.delete(mendaftar);
        }
        matkulRepository.delete(matkul);
    }
}
