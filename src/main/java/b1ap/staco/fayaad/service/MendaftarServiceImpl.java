package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MendaftarServiceImpl implements MendaftarService{
    @Autowired
    private MendaftarRepository mendaftarRepository;
    @Autowired
    private MatkulRepository matkulRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public Mendaftar createPendaftaran(Mendaftar mendaftar, Matkul matkul, User user) {
        for (Mendaftar mendaftarloop:user.getMatkulDidaftar()){
            if (mendaftarloop.getMatkul().getKode_matkul().equals(matkul.getKode_matkul())){
                return null;
            }
        }
        mendaftar = createMendaftar(mendaftar, matkul, user);
        addMendaftarToUser(mendaftar, user);
        addMendaftarToMatkul(mendaftar, matkul);
        return mendaftar;

    }

    @Override
    public Mendaftar createMendaftar(Mendaftar mendaftar, Matkul matkul, User user){
        mendaftar.setUser(user);
        mendaftar.setMatkul(matkul);
        mendaftar.setAsdos(false);
        mendaftar.setKode_asdos("");
        mendaftarRepository.save(mendaftar);
        return mendaftar;
    }

    @Override
    public void addMendaftarToUser(Mendaftar mendaftar, User user){
        user.getMatkulDidaftar().add(mendaftar);
        userRepository.save(user);
    }

    @Override
    public void addMendaftarToMatkul(Mendaftar mendaftar, Matkul matkul){
        matkul.getMatkulDidaftar().add(mendaftar);
        matkulRepository.save(matkul);
    }

    @Override
    public Mendaftar getMendaftar(String idmendaftar) {
        return mendaftarRepository.findByIdmendaftar(Integer.parseInt(idmendaftar));
    }


    @Override
    public Iterable<Mendaftar> getListMendaftar(Matkul matkul) {
        return matkul.getMatkulDidaftar();
    }


    @Override
    public Mendaftar getMendaftarByUserMatkul(User user, Matkul matkul) {
        Mendaftar result = null;
        for (Mendaftar mendaftar:getListMendaftar(matkul)){
            if (mendaftar.getUser().getUsername().equals(user.getUsername())){
                result = mendaftar;
            }
        }
        return result;
    }

    @Override
    public Mendaftar setAsdos(Mendaftar mendaftar) {
        mendaftar.setAsdos(true);
        mendaftarRepository.save(mendaftar);
        return mendaftar;
    }

    @Override
    public Mendaftar dropAsdos(Mendaftar mendaftar) {
        mendaftar.setAsdos(false);
        mendaftarRepository.save(mendaftar);
        return mendaftar;
    }

    @Override
    public Mendaftar addGroup(String idmahasiswa, String idasdos) {
        Mendaftar mahasiswa = getMendaftar(idmahasiswa);
        Mendaftar asdos = getMendaftar(idasdos);mahasiswa.setKode_asdos(asdos.getKode_asdos());
        mendaftarRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Mendaftar dropGroup(String idmahasiswa) {
        Mendaftar mahasiswa = getMendaftar(idmahasiswa);
        mahasiswa.setKode_asdos("");
        mendaftarRepository.save(mahasiswa);
        return mahasiswa;
    }

    @Override
    public Mendaftar setKodeAsdos(String idmendaftar, String kode_asdos) {
        Mendaftar mendaftar = getMendaftar(idmendaftar);
        String kode_asdos_old = mendaftar.getKode_asdos();
        mendaftar.setKode_asdos(kode_asdos);
        mendaftarRepository.save(mendaftar);
        if(!kode_asdos_old.equals("")){
            for (Mendaftar mendaftarEach:getListMendaftar(mendaftar.getMatkul())){
                mendaftarEach.setKode_asdos(kode_asdos);
                mendaftarRepository.save(mendaftarEach);
            }
        }
        return mendaftar;
    }

    @Override
    public void deleteMendaftar(String idmendaftar) {
        Mendaftar pendaftaran = getMendaftar(idmendaftar);
        User user = pendaftaran.getUser();
        user.getMatkulDidaftar().remove(pendaftaran);
        Matkul matkul = pendaftaran.getMatkul();
        matkul.getMatkulDidaftar().remove(pendaftaran);
        mendaftarRepository.delete(pendaftaran);
    }
}
