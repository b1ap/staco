package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;

public interface MatkulService {

    Iterable<Matkul> getListMatkul();

    Matkul createMatkul(Matkul matkul);

    Matkul getMatkul(String id);

    Iterable<Matkul> getMatkulByUsername(String username);

    void deleteMatkul(String id);
}
