//package b1ap.staco.controller;
//
//import b1ap.staco.ikhsan.controller.LoginController;
//import b1ap.staco.ikhsan.repository.UserRepository;
//import b1ap.staco.ikhsan.service.UserServiceImpl;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@WebMvcTest(controllers = LoginController.class)
//public class MainControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @MockBean
//    private UserServiceImpl userServiceImpl;
//
//    @Test
//    public void testHome() throws Exception {
//        mockMvc.perform(get("/"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void testJadwalAsis() throws Exception {
//        mockMvc.perform(get("/jadwalasis"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    @WithMockUser
//    public void testJadwalDemo() throws Exception {
//        mockMvc.perform(get("/jadwaldemo"))
//                .andExpect(status().isOk());
//    }
//}
