package b1ap.staco.fayaad.controller;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.service.MatkulServiceImpl;
import b1ap.staco.fayaad.service.MendaftarService;
import b1ap.staco.fayaad.service.MendaftarServiceImpl;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MatkulController.class)
public class MatkulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatkulServiceImpl matkulService;

    @MockBean
    private MendaftarServiceImpl mendaftarService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private Matkul matkul;

    @MockBean
    private User user;

    @MockBean
    private Mendaftar mendaftar;

    @BeforeEach
    public void setUp() {
        user = new User("admin", "test@gmail.com", "pass", "first", "last", false, "000");
        matkul = new Matkul("projut", "csge123");
        mendaftar = new Mendaftar(user, matkul, "YAD", true);
    }

    @Test
    @WithMockUser
    public void testgetListMatkul() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testgetListMatkulRedirect() throws Exception {
        mockMvc.perform(get("/matkul"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testJoinMatkulGet() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/join"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testJoinMatkulGetRedirect() throws Exception {
        mockMvc.perform(get("/matkul/join"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testGetMatkul() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(matkulService.getMatkul("0")).thenReturn(matkul);
        when(mendaftarService.getMendaftarByUserMatkul(user, matkul)).thenReturn(mendaftar);
        mockMvc.perform(get("/matkul/info/0"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testGetMatkulRedirect() throws Exception {
        mockMvc.perform(get("/matkul/info/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testAddGroup() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(matkulService.getMatkul("0")).thenReturn(matkul);
        when(mendaftarService.getMendaftarByUserMatkul(user, matkul)).thenReturn(mendaftar);
        mockMvc.perform(get("/matkul/info/0/add/1/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testAddGroupRedirect() throws Exception {
        mockMvc.perform(get("/matkul/info/0/add/1/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDropGroup() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(matkulService.getMatkul("0")).thenReturn(matkul);
        when(mendaftarService.getMendaftarByUserMatkul(user, matkul)).thenReturn(mendaftar);
        mockMvc.perform(get("/matkul/info/0/drop/1/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDropGroupRedirect() throws Exception {
        mockMvc.perform(get("/matkul/info/0/drop/1/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testLeaveMatkul() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/info/0/0/leave"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testLeaveMatkulRedirect() throws Exception {
        mockMvc.perform(get("/matkul/info/0/0/leave"))
                .andExpect(status().is3xxRedirection());
    }
}