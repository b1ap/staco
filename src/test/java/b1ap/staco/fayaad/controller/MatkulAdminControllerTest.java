package b1ap.staco.fayaad.controller;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.service.MatkulServiceImpl;
import b1ap.staco.fayaad.service.MendaftarService;
import b1ap.staco.fayaad.service.MendaftarServiceImpl;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MatkulAdminController.class)
public class MatkulAdminControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MatkulServiceImpl matkulService;

    @MockBean
    private MendaftarServiceImpl mendaftarService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private Matkul matkul;

    @MockBean
    private User user;

    @MockBean
    private Mendaftar mendaftar;

    @BeforeEach
    public void setUp() {
        user = new User("admin", "test@gmail.com", "pass", "first", "last", true, "000");
        matkul = new Matkul("projut", "csge123");
        mendaftar = new Mendaftar(user, matkul, "YAD", false);
    }

    @Test
    @WithMockUser
    public void testgetListMatkulDev() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/dev"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testgetListMatkulDevRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    void testgetListMatkulDevAPI() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/dev/api").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void testgetListMatkulDevAPIRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/api").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser
    public void testCreateMatkulDevGet() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/dev/create"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testCreateMatkulDevGetRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/create"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testGetMatkulDev() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(matkulService.getMatkul("0")).thenReturn(matkul);
        mockMvc.perform(get("/matkul/dev/info/0"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    public void testGetMatkulDevRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/info/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testSetAsdos() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(mendaftarService.getMendaftar("0")).thenReturn(mendaftar);
        mockMvc.perform(get("/matkul/dev/set/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testSetAsdosRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/set/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDropAsdos() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        when(mendaftarService.getMendaftar("0")).thenReturn(mendaftar);
        mockMvc.perform(get("/matkul/dev/drop/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDropAsdosRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/drop/0"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDeleteMatkul() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/matkul/dev/info/0/delete"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void testDeleteMatkulRedirect() throws Exception {
        mockMvc.perform(get("/matkul/dev/info/0/delete"))
                .andExpect(status().is3xxRedirection());
    }
}