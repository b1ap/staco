package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MatkulServiceImplTest {
    @Mock
    private MatkulRepository matkulRepository;

    @Mock
    private MendaftarRepository mendaftarRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private MatkulServiceImpl matkulService;

    private User user;

    private Matkul matkul;

    private Matkul matkul2;

    private Mendaftar mendaftar;


    @BeforeEach
    public void setUp() {
        user = new User("user", "test@gmail.com", "pass", "first", "last", false, "111");
        matkul = new Matkul("projut", "csge123");
        matkul2 = new Matkul("ppw", "csge456");
        mendaftar = new Mendaftar(user, matkul, "VAN", false);
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetMatkuls(){
        List<Matkul> matkulList = matkulRepository.findAll();
        lenient().when(matkulService.getListMatkul()).thenReturn(matkulList);
        Iterable<Matkul> resultMatkulList = matkulService.getListMatkul();
        Assertions.assertIterableEquals(matkulList, resultMatkulList);
    }

    @Test
    public void testCreateMatkul() {
        Iterable matkuls = Arrays.asList(matkul2);
        lenient().when(matkulService.getListMatkul()).thenReturn(matkuls);
        Matkul resultMatkul = matkulService.createMatkul(matkul);
        Assertions.assertEquals(matkul.getKode_matkul(), resultMatkul.getKode_matkul());
    }

    @Test
    public void testCreateMatkulAlready() {
        Iterable matkuls = Arrays.asList(matkul);
        lenient().when(matkulService.getListMatkul()).thenReturn(matkuls);
        Matkul resultMatkul = matkulService.createMatkul(matkul);
        Assertions.assertEquals(null, resultMatkul);
    }

    @Test
    public void testGetMatkulByID(){
        lenient().when(matkulService.getMatkul("0")).thenReturn(matkul);
        Matkul resultMatkul = matkulService.getMatkul(String.valueOf(matkul.getID()));
        assertEquals(matkul.getID(), resultMatkul.getID());
    }

    @Test
    public void testGetMatkulListByUsers(){
        user.getMatkulDidaftar().add(mendaftar);
        userRepository.save(user);
        List<Matkul> matkulListCorrect = Arrays.asList(matkul);
        lenient().when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        Iterable<Matkul> result = matkulService.getMatkulByUsername(user.getUsername());
        assertEquals(matkulListCorrect, result);
    }

    @Test
    public void testDeleteMatkul(){
        matkulService.createMatkul(matkul);
        when(matkulRepository.findByID(matkul.getID())).thenReturn(matkul);
        matkul.getMatkulDidaftar().add(mendaftar);
        matkulService.deleteMatkul(String.valueOf(matkul.getID()));
        when(matkulRepository.findByID(matkul.getID())).thenReturn(null);
        Assertions.assertEquals(null, matkulService.getMatkul(String.valueOf(matkul.getID())));
    }

}