package b1ap.staco.fayaad.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MendaftarServiceImplTest {

    @Mock
    private MatkulRepository matkulRepository;

    @Mock
    private MendaftarRepository mendaftarRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private MendaftarServiceImpl mendaftarService;

    private User user;

    private User asdos;

    private Matkul matkul;

    private Matkul matkul2;

    private Mendaftar mendaftar;

    private Mendaftar mendaftar2;

    private Mendaftar mendaftar3;


    @BeforeEach
    public void setUp() {
        user = new User("user", "test@gmail.com", "pass", "first", "last", false, "111");
        asdos = new User("asdos", "test@gmail.com", "pass", "first", "last", false, "222");
        matkul = new Matkul("projut", "csge123");
        matkul2 = new Matkul("ppw", "csge666");
        mendaftar = new Mendaftar(user, matkul, "VAN", false);
        mendaftar2 = new Mendaftar();
        mendaftar3 = new Mendaftar(asdos, matkul, "DOO", true);
        MockitoAnnotations.openMocks(this);

    }

    @Test
    public void testGetMendaftar(){
        lenient().when(mendaftarRepository.findByIdmendaftar(0)).thenReturn(mendaftar);
        lenient().when(mendaftarService.getMendaftar("0")).thenReturn(mendaftar);
    }

    @Test
    public void testGetListMendaftarByMatkul(){
        List<Mendaftar> mendaftarList = mendaftarRepository.findAll();
        lenient().when(mendaftarService.getListMendaftar(matkul)).thenReturn(mendaftarList);
        Iterable<Mendaftar> resultMendaftarList = mendaftarService.getListMendaftar(matkul);
        Assertions.assertIterableEquals(mendaftarList, resultMendaftarList);
    }

    @Test
    public void testCreateMendaftar() {
        mendaftar.setMatkul(matkul);
        mendaftarRepository.save(mendaftar);
        user.getMatkulDidaftar().add(mendaftar);
        userRepository.save(user);
        lenient().when(mendaftarService.createPendaftaran(mendaftar2, matkul2, user)).thenReturn(mendaftar2);
    }

    @Test
    public void testCreateMendaftarAlready() {
        mendaftar2.setMatkul(matkul2);
        mendaftarRepository.save(mendaftar2);
        user.getMatkulDidaftar().add(mendaftar2);
        userRepository.save(user);
        lenient().when(mendaftarService.createPendaftaran(mendaftar2, matkul2, user)).thenReturn(null);
    }

    @Test
    public void testGetMendaftarByUserMatkul() {
        matkul.getMatkulDidaftar().add(mendaftar);
        matkulRepository.save(matkul);
        lenient().when(mendaftarService.getMendaftarByUserMatkul(user, matkul)).thenReturn(mendaftar);
    }


    @Test
    public void testSetAsdos() {
        lenient().when(mendaftarService.setAsdos(mendaftar2)).thenReturn(mendaftar2);
    }

    @Test
    public void testDropAsdos() {
        lenient().when(mendaftarService.dropAsdos(mendaftar2)).thenReturn(mendaftar2);
    }


    @Test
    public void testAddGroup() {
        mendaftarRepository.save(mendaftar);
        mendaftarRepository.save(mendaftar3);
        when(mendaftarRepository.findByIdmendaftar(0)).thenReturn(mendaftar);
        when(mendaftarRepository.findByIdmendaftar(2)).thenReturn(mendaftar3);
        mendaftarService.addGroup("0", "2");
        Assertions.assertEquals(mendaftar.getKode_asdos(), mendaftar3.getKode_asdos());
    }

    @Test
    public void testDropGroup() {
        mendaftarRepository.save(mendaftar);
        when(mendaftarRepository.findByIdmendaftar(0)).thenReturn(mendaftar);
        mendaftarService.dropGroup("0");
        Assertions.assertEquals("", mendaftar.getKode_asdos());
    }

    @Test
    public void testSetKodeAsdos() {
        matkul.getMatkulDidaftar().add(mendaftar3);
        matkulRepository.save(matkul);
        mendaftarRepository.save(mendaftar3);
        when(mendaftarRepository.findByIdmendaftar(2)).thenReturn(mendaftar3);
        mendaftarService.setKodeAsdos("2", "BARU");
        Assertions.assertEquals("BARU", mendaftar3.getKode_asdos());
    }

    @Test
    public void testDeleteMendaftar(){
        mendaftarService.createPendaftaran(mendaftar, matkul, user);
        when(mendaftarRepository.findByIdmendaftar(0)).thenReturn(mendaftar);
        mendaftarService.deleteMendaftar("0");
        when(mendaftarRepository.findByIdmendaftar(0)).thenReturn(null);
        Assertions.assertEquals(null, mendaftarService.getMendaftar("0"));
    }

}