package b1ap.staco.ikhsan.repository;

import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserRepositoryTest {

    @Mock
    UserRepository userRepository;

//    @MockBean
//    private User testUser;
//
//    @BeforeEach
//    public void setUp() {
//        testUser = new User("test", "test", "test",
//                "test", "test@test", false, "123");
//        userRepository.save(testUser);
//    }

    @Test
    public void testFindByUsername() {
        assertThat(userRepository.findByUsername("test")).isNull();
    }
}
