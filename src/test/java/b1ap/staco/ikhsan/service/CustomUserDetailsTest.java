package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CustomUserDetailsTest {

    @InjectMocks
    private CustomUserDetails customUserDetails;

    @MockBean
    private User testUser;

    @BeforeEach
    public void setUp() {
        testUser = new User("test", "test", "test",
                "test", "test@test", false, "123");
    }

    @Test
    public void testCustomUserDetails() {
        customUserDetails = new CustomUserDetails(testUser);
    }

    @Test
    public void testGetAuthorities() {
        assertThat(customUserDetails.getAuthorities()).isNull();
    }

    @Test
    public void testGetPasswordAndUsername() {
        customUserDetails = new CustomUserDetails(testUser);

        assertThat(customUserDetails.getPassword()).isNotNull();
        assertThat(customUserDetails.getUsername()).isNotNull();
    }

    @Test
    public void testOthers() {
        assertThat(customUserDetails.isAccountNonExpired()).isEqualTo(true);
        assertThat(customUserDetails.isAccountNonLocked()).isEqualTo(true);
        assertThat(customUserDetails.isEnabled()).isEqualTo(true);
        assertThat(customUserDetails.isCredentialsNonExpired()).isEqualTo(true);
    }
}
