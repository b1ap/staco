package b1ap.staco.ikhsan.service;

import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class WebSecurityConfigTest {

    @InjectMocks
    private WebSecurityConfig webSecurityConfig;

    @Test
    public void testUserDetailsService() {
        assertThat(webSecurityConfig.userDetailsService()).isNotNull();
    }

    @Test
    public void testPasswordEncoder() {
        assertThat(webSecurityConfig.passwordEncoder()).isNotNull();
    }

    @Test
    public void testAuthenticationProvider() {
        assertThat(webSecurityConfig.authenticationProvider()).isNotNull();
    }

}
