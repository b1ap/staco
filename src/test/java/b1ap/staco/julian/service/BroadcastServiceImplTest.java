package b1ap.staco.julian.service;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.fayaad.model.Mendaftar;
import b1ap.staco.fayaad.repository.MatkulRepository;
import b1ap.staco.fayaad.repository.MendaftarRepository;
import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import b1ap.staco.julian.core.Broadcast;
import b1ap.staco.julian.repository.BroadcastRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.lenient;

public class BroadcastServiceImplTest {
    @Mock
    private BroadcastRepository broadcastRepository;

    @Mock
    private MatkulRepository matkulRepository;

    @Mock
    private MendaftarRepository mendaftarRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private BroadcastServiceImpl broadcastService;

    private User user;
    private Matkul matkul;
    private Mendaftar mendaftar;
    private Broadcast broadcast;

    @BeforeEach
    public void setUp() {
        user = new User("user", "test@gmail.com", "pass", "first", "last", false, "111");
        matkul = new Matkul("projut", "csge123");
        broadcast = new Broadcast();
        broadcast.setMataKuliah(matkul);
        broadcast.setMessage("Testing Message");
        broadcast.setUser(user);
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllBroadcastByMataKuliah() {
        List<Broadcast> allBroadcast = broadcastRepository.findAllByMataKuliah(matkul);
        lenient().when(broadcastService.getAllBroadcastByMataKuliah(matkul)).thenReturn(allBroadcast);
        Iterable<Broadcast> resultAllBroadcast = broadcastService.getAllBroadcastByMataKuliah(matkul);
        assertIterableEquals(allBroadcast, resultAllBroadcast);
    }

    @Test
    public void testCreateBroadcast() {
        List<Broadcast> broadcasts = new ArrayList<>();
        broadcasts.add(broadcast);
        lenient().when(broadcastService.getAllBroadcastByMataKuliah(matkul)).thenReturn(broadcasts);
        Broadcast resultBroadcast = broadcastService.createBroadcast(broadcast);
        assertEquals(broadcast.getBroadcastId(), resultBroadcast.getBroadcastId());
    }

    @Test
    public void testGetUserInfo(){
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        assertEquals("first last - 111", broadcastService.getUserInfoByUsername("user"));
    }

    @Test
    public void testGetUser(){
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        assertEquals(user, broadcastService.getUserByUsername("user"));
    }

    @Test
    public void testGetMatkul(){
        lenient().when(matkulRepository.findByID(0)).thenReturn(matkul);
        assertEquals(matkul, broadcastService.getMatkulByID(0));
    }

    @Test
    public void testGetMatkulByUser(){
        lenient().when(userRepository.findByUsername("user")).thenReturn(user);
        List<Matkul> result = new ArrayList<>();
        assertEquals(result, broadcastService.getAllMatkulByUsername("user"));
    }

}
