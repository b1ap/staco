package b1ap.staco.julian.core;

import b1ap.staco.fayaad.model.Matkul;
import b1ap.staco.ikhsan.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class BroadcastTest {
    @MockBean
    private User user;

    @MockBean
    private Matkul matkul;

    @BeforeEach
    public void setUp() {
        user = new User("admin", "test@gmail.com", "pass", "first", "last", false, "000");
        matkul = new Matkul();
        matkul.setID(10);
        matkul.setNama_matkul("Alin");
        matkul.setKode_matkul("CSCM123");
    }

    @Test
    public void testSetterAndGetterBroadcast() {
        Broadcast broadcast = new Broadcast();

        broadcast.setMessage("Testing Message");
        broadcast.setUser(user);
        broadcast.setMataKuliah(matkul);
        broadcast.setTitle("judul");

        assertNotNull(broadcast.getBroadcastId());
        assertNotNull(broadcast.getMataKuliah());
        assertNotNull(broadcast.getMessage());
        assertNotNull(broadcast.getUser());
        assertEquals(0, broadcast.getBroadcastId());
        assertEquals(matkul, broadcast.getMataKuliah());
        assertEquals("Testing Message", broadcast.getMessage());
        assertEquals(user, broadcast.getUser());
        assertEquals("judul", broadcast.getTitle());
    }
}
