package b1ap.staco.julian.controller;

import b1ap.staco.ikhsan.model.User;
import b1ap.staco.ikhsan.repository.UserRepository;
import b1ap.staco.julian.service.BroadcastServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = BroadcastController.class)
public class BroadcastControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BroadcastServiceImpl broadcastService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private User user;

    @BeforeEach
    public void setUp() {
        user = new User("admin", "test@gmail.com", "pass", "first", "last", false, "000");
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @WithMockUser
    public void whenBroadcastHomeURLIsAccessedItShouldContainAllMatkulAllBroadcastAndIsAsdos() throws Exception {
        when(userRepository.findByUsername(anyString())).thenReturn(user);
        mockMvc.perform(get("/broadcast/"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getListMataKuliah"))
                .andExpect(model().attributeExists("allMatkul"))
                .andExpect(model().attributeExists("allBroadcast"))
                .andExpect(model().attributeExists("isAsdos"));
    }

}
